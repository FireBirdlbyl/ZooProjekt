package interfaces;

public interface Soundable {
    int id = 0;
    void sound();
}