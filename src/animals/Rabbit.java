package animals;

import input.Input;
import meta.FillRetention;

@FillRetention
public class Rabbit extends Herbivore {
    public Rabbit(Double size, String nickname) {
        super(size, nickname);
        type = Rabbit.class.getSimpleName();
    }

    public static Rabbit createRabbit() {
        double size = Input.megaInputNumber("Enter your rabbit's size");
        String name = Input.megaInputString("Enter your rabbit's name");
        return new Rabbit(size, name);
    }

    @Override
    public double jump() {
        return 0;
    }

    @Override
    public void sound() {

    }
}
