package animals;

public abstract class Cannine extends Predator {
    public Cannine(double size, String nickname) {
        super(size, nickname);
    }
}
